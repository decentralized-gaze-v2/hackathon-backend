const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');//connecting to mongoose
const cors = require('cors');


// Importing Routes
const authUserRoute = require('./routes/authUser');
const itemRoute = require('./routes/item/item');
dotenv.config();
//connecting to db 
mongoose.connect(process.env.DB_CONNECT,
    { useNewUrlParser: true, useUnifiedTopology: true }
    , () => console.log('Connected to server !')
);
app.use(cors());
//Json request/response for middleware
app.use(express.json());

// Route Middleware
app.use('/api/auth', authUserRoute);
app.use('/api/item', itemRoute);

app.get('/', function (req, res) {
    res.send('Decentralized gaze!');
  });
app.use(function(req, res) {
    res.send('Decentralized gaze!');
    res.redirect('/')
});
app.listen(process.env.PORT || 3000, () => console.log('Server is up and running :) :) '));