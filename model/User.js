const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 40
    },
    address: {
        type: String,
        required: true,
        min: 3,
    },
    phone: {
        type: String,
        required: true,
        min: 11,
        max: 11
    },
    username: {
        type: String,
        required: true,
        min: 6,
        max: 15
    },
    password: {
        type: String,
        required: true,
        min: 6
    },
    email: {
        type: String,
        required: true,
    },
    rating: {
        type: Number,
    },
    verified: {
        type: Boolean,
        required: true
    },
    active: {
        type: Boolean,
        required: true,
    }

});

module.exports = mongoose.model('User', userSchema);   