const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    category: { // game console , gadgets , hobbies
        type: String,
        required: true,
    },
    uid: { // user id 
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    date_posted: {
        type: Date,
        required: true,
    },
    type: {
        type: Boolean, /// lending == true  || borrowing  == false
        required: true,
    },
    item_price: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Item', itemSchema);   