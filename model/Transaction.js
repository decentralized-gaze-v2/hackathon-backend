const mongoose = require('mongoose');

const itemTransactionSchema = new mongoose.Schema({
    item_id: {
        type: String,
        required: true,
    },
    borrower_uid: {
        type: String,
        required: true,
    },
    start_date: {
        type: Date,
        required: true,
    },
    end_date: {
        type: Date,
        required: true,
    },
    status: {
        type: Boolean,
        required: true,
    }
});

module.exports = mongoose.model('ItemTransaction', itemTransactionSchema);   