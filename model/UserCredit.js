const mongoose = require('mongoose');

const userCreditSchema = new mongoose.Schema({
    uid: {
        type: String,
        required: true,
    },
    balance: {
        type: Number,
        required: true
    }

});

module.exports = mongoose.model('UserCredit', userCreditSchema);   