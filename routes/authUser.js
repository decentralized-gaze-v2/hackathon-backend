const router = require('express').Router();
const User = require("../model/User");
const UserCredit = require("../model/UserCredit");
const bcrypt = require("bcryptjs");

const userValidation = require("../routes/validation/authUserValidation");


// temporary 
router.post('/register', async (req, res) => {
    const { error } = userValidation.registerValidation(req.body);
    //checking validation
    if (error) return res.status(400).send(error.details[0].message);
    //checking if student is already exist
    const usernameExist = await User.findOne({ username: req.body.username });

    if (usernameExist) return res.status(400).send('Username is Already Exist !');

    const salt = await bcrypt.genSalt(10);
    const hashedpassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name: req.body.name,
        address: req.body.address,
        phone: req.body.phone,
        username: req.body.username,
        password: hashedpassword,
        email: req.body.email,
        rating : 0 , // set as 0 since new user
        verified: true, // if not verified the user cant proceed on borrowing part
        active: true, // not advisable on production for hackathon purpose only lol.
    })

    try {
        const saveUser = await user.save();
        const userCredit = new UserCredit({
            uid: user._id,
            balance:2000 // dummy for presentation purposes
        })
        const saveUserCredit = await userCredit.save();
        res.send({saveUser,saveUserCredit});
    } catch (error) {
        res.status(400).send(error);
    }
});

// login 


router.post('/login', async (req, res) => {
    const { error } = userValidation.loginValidation(req.body);

    if (error) return res.status(400).send(error.details[0].message);
    //check if email exist
    const user = await User.findOne({ username: req.body.username });
    //if invalid email/not found
    if (!user) return res.status(400).send('Username not found');

    const userBalance = await UserCredit.findOne({ uid: user._id });

    //check student pass if correct
    const validPass = await bcrypt.compare(req.body.password, user.password);
    //invalid pass prompt
    if (!validPass) return res.status(400).send('Invalid Password !');
    const userDetails = {
        username: user.username,
        name: user.name,
        phone: user.phone,
        email: user.email,
        active: user.active,
        rating:user.rating,
        balance:userBalance.balance,
        isLoggedIn: true,
        uid: user._id
    };
    res.send(userDetails);
});




module.exports = router;