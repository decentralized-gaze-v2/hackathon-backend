const router = require('express').Router();
const Item = require("../../model/Item");


const itemValidation = require("../../routes/validation/itemValidation");


//  Borrow / Lend  Post Process
router.post('/post', async (req, res) => {
    const { error } = itemValidation.postValidation(req.body);
    //checking validation
    if (error) return res.status(400).send(error.details[0].message);
    const item = new Item({
        category: req.body.category, // sort
        uid: req.body.uid,
        title: req.body.title,
        description: req.body.description,
        date_posted: req.body.date_posted,
        type: req.body.type, // sort // true if lending || false if borrowing
        item_price: req.body.item_price
    })
    try {
        const saveItem = await item.save();
        const response = {
            message: "Successfully Posted Request"
        }
        res.json({ response, saveItem });
    } catch (error) {
        res.status(400).send(error);
    }

});
// Sort Item based on category Type
router.post('/category', async (req, res) => {
    const categorizedItem = await Item.find({ category: { "$in": req.body.data },uid: { $ne: req.body.uid } });
    res.send(categorizedItem);
});

// Sort Item based on category Type
router.post('/all', async (req, res) =>  {
    const itemList = await Item.find({ uid: { $ne: req.body.uid } });
    res.send(itemList);
});

// Sort Item based on Type 
router.post('/type', async (req, res) => {
    const categorizedItem = await Item.find({ type: { "$in": req.body.data },uid: { $ne: req.body.uid } });
    res.send(categorizedItem);
});



module.exports = router;