const Joi = require('@hapi/joi');

const registerValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string()
            .min(3)
            .max(40)
            .required(),
        address: Joi.string()
            .min(3)
            .required(),
        phone: Joi.string()
            .min(11)
            .max(11)
            .required(),
        username: Joi.string()
            .min(6)
            .required(),
        password: Joi.string()
            .min(6)
            .required(),
        email: Joi.string()
            .min(20)
            .required()
    });
    return schema.validate(data);
};

const loginValidation = (data) => {
    const schema = Joi.object({
        username: Joi.string()
            .min(6)
            .required(),
        password: Joi.string()
            .min(6)
            .required(),
    });
    return schema.validate(data);
};
module.exports = { loginValidation, registerValidation }