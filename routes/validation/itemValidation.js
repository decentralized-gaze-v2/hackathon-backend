const BaseJoi = require('@hapi/joi');
const Extension = require('@hapi/joi-date');
const Joi = BaseJoi.extend(Extension);

const postValidation = (data) => {
    const schema = Joi.object({
        category: Joi.string()
            .required(),
        uid: Joi.string()
            .required(),
        title: Joi.string()
            .required(),
        description: Joi.string()
            .required(),
        date_posted: Joi.date()
            .format('YYYY-MM-DD')
            .required(),
        type: Joi.boolean()
            .required(),
        item_price: Joi.number()
            .required()

    });
    return schema.validate(data);
};

module.exports = { postValidation }